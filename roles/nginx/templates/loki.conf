map $http_upgrade $connection_upgrade {
  default upgrade;
  '' close;
}

server {
    listen 80;
    listen [::]:80;
    server_name {{ loki.domain }};
    # enforce https
    return 302 https://{{ loki.domain }}:443$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name {{ loki.domain }};
    ssl_certificate /etc/letsencrypt/live/{{ loki.domain }}/cert.pem;
    ssl_certificate_key /etc/letsencrypt/live/{{ loki.domain }}/privkey.pem;
    auth_basic "shift-rmm logs";
    auth_basic_user_file /etc/nginx/htpasswd;
    add_header X-Robots-Tag none;
    add_header Strict-Transport-Security "max-age=15552000; includeSubDomains" always;
    gzip on;
    gzip_vary on;
    gzip_comp_level 4;
    gzip_min_length 256;
    gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;
    gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;

    location / {
        proxy_pass http://localhost:3100;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header Connection "Keep-Alive";
        proxy_set_header Proxy-Connection "Keep-Alive";
        proxy_redirect off;
    }
}
